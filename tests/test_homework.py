import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.projects_page import ProjectsPage
from pages.add_project_page import AddProjectPage
from pages.project_details_page import ProjectDetailsPage

from utils.random import RandomUtil


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()


def test_add_new_project(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    # Check if the projects page has been loaded
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == "Projekty"

    projects_page = ProjectsPage(browser)
    projects_page.click_add_project()
    # Check if the add project page has been loaded
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == "Dodaj projekt"

    add_project_page = AddProjectPage(browser)
    project_prefix = RandomUtil.get_random_string(5)
    project_name = f"4_testers {project_prefix}"
    add_project_page.add_project(project_name, project_prefix)
    # Check if the add project confirmation box is displayed
    assert browser.find_element(By.CSS_SELECTOR, '#j_info_box').is_displayed()

    project_details_page = ProjectDetailsPage(browser)
    project_details_page.close_new_project_confirmation()
    project_details_page.click_cockpit()

    cockpit_page.click_administration()
    # Check if the projects page has been loaded
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == "Projekty"

    projects_page.find_project(project_name)
    # Check if the project has been found - single check by unique project prefix
    assert browser.find_element(By.CSS_SELECTOR, 'table > tbody tr:first-child > .t_number').text == project_prefix
