import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture()
def browser():
    # Faza 1 - coś co się wykona przed każdym testem
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://www.awesome-testing.com')

    # Faza 2 - coś co przekazujemy do każdego testu
    yield browser

    # Faza 3 - coś co się wykona po każdym teście
    browser.quit()


def test_post_count(browser):
    # Pobranie listy tytułów
    post_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 4 elementy
    assert len(post_titles) == 4


def test_post_count_after_search(browser):
    # Inicjalizacja searchbara i przycisku search
    search_box_input = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button_input = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_box_input.is_displayed()
    assert search_button_input.is_displayed()

    # Szukanie
    search_box_input.send_keys('Cypress')
    search_button_input.click()

    # Pobranie listy tytułów
    post_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 5 elementy
    assert len(post_titles) == 5


def test_post_count_on_cypress_label(browser):
    # Inicjalizacja elementu z labelką
    cypress_label = browser.find_element(By.CSS_SELECTOR, 'a[href*="cypress"]')

    # Kliknięcie na labelkę
    cypress_label.click()

    # Pobranie listy tytułów
    post_titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 1 element
    assert len(post_titles) == 1
