from selenium.webdriver.common.by import By


class AddProjectPage:
    def __init__(self, browser):
        self.browser = browser

    '''
    Add project method.
    Parameters:
        - name - mandatory field
        - prefix - mandatory field
        - description - non-mandatory field, empty default value
    '''
    def add_project(self, name, prefix, description=""):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(name)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(prefix)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(description)

        self.browser.find_element(By.CSS_SELECTOR, '#save').click()
