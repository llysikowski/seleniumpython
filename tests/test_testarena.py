import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.panel_page import PanelPage

from utils.random import RandomUtil


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # LoginPage class object creation (page object)
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()


def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True


def test_open_messages(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()
    message_panel = browser.find_element(By.CSS_SELECTOR, '#list-message')
    assert message_panel.is_displayed()


def test_open_messages_with_wait(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()

    panel_page = PanelPage(browser)
    active_response_button = panel_page.wait_for_load()

    assert active_response_button.is_displayed()


def test_add_message(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_envelope()

    panel_page = PanelPage(browser)
    panel_page.wait_for_load()

    panel_page.send_message(RandomUtil.get_random_string(20))


def test_open_admin_panel(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == "Projekty"
